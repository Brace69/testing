module NetScaler::LoadBalancer

  class Service
    
    attr_accessor :name, :protocol, :port

    def initialize name, protocol, port
      @name, @protocol, @port = name, protocol, port
      @servers, @monitor= [], []
    end

    def addServer server
      @servers.push server
    end

    def getServers
      return @servers
    end

    def cliOutput
      string = ""
      string << "add service #{@name} #{@protocol} #{@port}\n"
      @servers.each do |server|
        string << "bind #{@name} #{server.name}\n"
      end
      return string
    end

  end

end
