module NetScaler::Network

  class Route

    attr_accessor :subnet, :netmask, :gateway

    def initialize subnet, netmask, gateway
      @subnet, @netmask, @gateway = subnet, netmask, gateway
    end
    
  end

end
