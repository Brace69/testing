module NetScaler::Network

  class Vlan

    attr_accessor :id, :name

    def initialize id, name=nil
      @id, @name = id, name
      @interfaces, @addresses = [], []
    end

    def addInterface interface
      @interfaces.push interface
    end

    def getInterfaces
      return @interfaces
    end

    def addAddress address, tagged = false
      @addresses.push({"address": address, "tagged": tagged})
    end

    def getAddresses
      return @addresses
    end

  end

end
