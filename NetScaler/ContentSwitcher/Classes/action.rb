module NetScaler::ContentSwitcher

  class Action

    attr_accessor :name, :vserver

    def initialize name, vserver
      @name, @vserver = name, vserver
    end

  end

end
