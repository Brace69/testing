module NetScaler::LoadBalancer

  require_relative 'Classes/server'
  require_relative 'Classes/monitor'
  require_relative 'Classes/service'
  require_relative 'Classes/servicegroup'
  require_relative 'Classes/vserver'

end
