module NetScaler::LoadBalancer

  class ServiceGroup
    attr_accessor :name, :protocol
    def initialize name, protocol
      @name, @protocol = name, protocol
      @servers = []
    end
    def addServer server, port
      @servers.push({"server": server, "port": port})
    end
    def getServers
      return @servers
    end
  end

end
