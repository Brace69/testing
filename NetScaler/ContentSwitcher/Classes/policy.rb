module NetScaler::ContentSwitcher

  class Policy

    attr_accessor :name, :rule, :action

    def initialize name, rule, action=nil
      @name, @rule, @action = name, rule, action
    end
    
  end

end
