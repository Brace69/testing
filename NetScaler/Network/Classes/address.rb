module NetScaler::Network

  class Address

    attr_accessor :ip, :netmask

    def initialize ip, netmask
      @ip, @netmask = ip, netmask
    end

  end

  class ManagementAddress < Address

    def initialize ip, netmask
      super ip, netmask
    end

  end

  class SubnetAddress < Address

    def initialize ip, netmask
      super ip, netmask
    end

  end

  class VirtaulAddress < Address

    def initialize ip, netmask
      super ip, netmask
    end

  end

end
