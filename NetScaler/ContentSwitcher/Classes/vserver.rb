module NetScaler::ContentSwitcher

  class VServer

    attr_accessor :name, :protocol, :port

    def initialize name, protocol, port
      @name, @protocol, @port = name, protocol, port
      @policies = []
    end

    def addPolicy name, action=nil
      @policies.push({"name": name, "action": action})
    end

    def getPolicies
      return @policies
    end
    
  end

end
