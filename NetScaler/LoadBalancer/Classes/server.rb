module NetScaler::LoadBalancer

  require 'resolv'

  class Server
    attr_accessor :name, :address, :comment
    def initialize name, comment=nil
      @name, @comment = name, comment
    end
    def outputCli
      return "add server #{@name} #{@address}" if @comment.nil?
      return "add server #{@name} #{@address} -comment #{@comment}"
    end
  end

  class IPServer < Server

    def initialize name, ip, comment=nil
      super name, comment
      @address = IPAddr.new(ip) if ip.is_a? String
      @address = address if ip.is_a? IPAddr
    end

  end

  class DNSServer < Server


    def initialize name, record, comment=nil
      super name, comment
      @record = record
      # Get dns address(es)
      #@address = Resolv::DNS.new.getresources @record, Resolv::DNS::Resource::IN::A
      @address = @record
    end

    def updateRecord
      #@address = Resolv::DNS.new @record
      @address = @record
    end

  end

end
