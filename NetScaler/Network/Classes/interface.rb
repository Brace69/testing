module NetScaler::Network

  class Interface

    attr_accessor :ifnum, :intftype, :tagall

    def initialize ifnum, intftype, tagall
      @ifnum, @intftype, @tagall = ifnum, intftype, tagall
    end

  end

end
