module NetScaler::LoadBalancer

  class VServer
    attr_accessor :name, :protocol, :port
    def initialize name, protocol, port
      @name, @protocol, @port = name, protocol, port
      @services, @service_group = [], []
    end
    def addService service
      @services.push service
    end
    def getServices
      return @services
    end
    def addServiceGroup service_group
      @service_groups.push service_group
    end
    def getServiceGroups
      return @service_groups
    end
  end

end
