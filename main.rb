require_relative 'NetScaler/netscaler'

if ARGV.length != 1 then
  puts "Too few arguments, ruby main.rb <config file>"
  return 0
end

NS = NetScaler
LB = NS::LoadBalancer
CS = NS::ContentSwitcher
NWK = NS::Network

config_servers = []
config_services = []
config_services_groups = []
config_lb_vservers = []

config_file = File.open ARGV[0], "r"


config_file.each do |line|

  # Get all LB Servers
  if match = line.match(/^add server (".+?"|.+?) (.+?)(?: -comment (.+?))?$/) then
    name, address, comment = match.captures[0], match.captures[1], match.captures[2]
    config_servers.push LB::DNSServer.new name, address, comment unless address =~ (/^\d+?\.\d+?\.\d+?\.\d+?$/)
    config_servers.push LB::IPServer.new name, address, comment if address =~ (/^\d+?\.\d+?\.\d+?\.\d+?$/)
  end
  # if match = line.match(/^add service (.+?) (.+?) (.+?) (.+?)$/) then
  #   config_services.push LB::Service.new match.captures[0], match.captures[1], match.captures[2].to_i
  # end

end

config_servers.each {|server| puts server.outputCli}
config_services.each {|server| puts server.name}
